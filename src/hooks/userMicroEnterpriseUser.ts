import { ref, onMounted, onUnmounted, watch, reactive } from 'vue'
import { Storage } from '@/utils/Storage'
import { useStore } from '@/store'
import { toRefs } from '@vueuse/shared'
interface TipsConfig {
  content: string
  link: string
}
interface MnState {
  isQWTips: boolean
  wechatFlag: number
  tipsData: TipsConfig
  isMicroEnterpriseUsers: boolean
}
export const userMicroEnterpriseUser = () => {
  const store = useStore()
  const { wechatFlag, wechatFlagContent, wechatFlagLink } = Storage.get('pageConfig')
  const state = reactive<MnState>({
    isQWTips: false,
    wechatFlag,
    tipsData: {
      content: wechatFlagContent,
      link: wechatFlagLink
    },
    isMicroEnterpriseUsers: false
  })
  onMounted(async () => {
    if (state.wechatFlag == 1) {
      state.isMicroEnterpriseUsers = await store.dispatch('user/getMicroEnterpriseUsers')
      state.isQWTips = !state.isMicroEnterpriseUsers
      console.log(
        '%c 🦀 isMicroEnterpriseUsers: ',
        'font-size:16px;background-color: #F5CE50;color:#fff;',
        state.isMicroEnterpriseUsers
      )
    }
  })
  return {
    ...toRefs(state)
  }
}
