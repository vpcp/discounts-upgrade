import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'bordereaux'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/bordereaux',
    name: routeName,
    redirect: '/bordereaux/discountCoupon',
    component: RouterTransition,
    meta: {
      title: '珠海春节营销活动',
      icon: 'iconquxiantu'
    },
    children: [
      {
        path: 'discountCoupon',
        name: `${routeName}-discountCoupon`,
        meta: {
          title: '按钮点击统计',
          icon: 'iconyouhuiquan'
        },
        component: () =>
          import(
            /* webpackChunkName: "dashboard-welcome" */ '@/views/background/bordereaux/discountCoupon.vue'
          )
      },
      {
        path: 'ECOPHistory',
        name: `${routeName}-ECOPHistory`,
        meta: {
          title: '问卷调查统计',
          icon: 'iconyouhuiquanpaifafenxi'
        },
        component: () =>
          import(
            /* webpackChunkName: "dashboard-welcome" */ '@/views/background/bordereaux/ECOPHistory.vue'
          )
      },
      {
        path: 'logHistory',
        name: `${routeName}-logHistory`,
        meta: {
          title: '套餐办理统计',
          icon: 'iconyouhuiquanpaifafenxi'
        },
        component: () =>
          import(
            /* webpackChunkName: "dashboard-welcome" */ '@/views/background/bordereaux/logHistory.vue'
          )
      }
    ]
  }
]

export default routes
