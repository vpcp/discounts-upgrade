import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/weChatActivity',
    name: 'weChatActivity',
    component: () =>
      import(/* webpackChunkName: "weChatActivity" */ '@/views/mobile/weChatActivity/index.vue'),
    meta: {
      title: '珠海春季营销购机',
      icon: 'iconquxiantu',
      hasLogin: false
    }
  },
  // 二级页面
  {
    path: '/particulars',
    name: 'particulars',
    component: () =>
      import(/* webpackChunkName: "weChatActivity" */ '@/views/mobile/particulars/index.vue'),
    meta: {
      title: '详情',
      icon: 'iconquxiantu',
      hasLogin: false
    }
  },
  // 手表-watches
  {
    path: '/watches',
    name: 'watches',
    component: () =>
      import(/* webpackChunkName: "weChatActivity" */ '@/views/mobile/particulars/watches.vue'),
    meta: {
      title: '详情',
      icon: 'iconquxiantu',
      hasLogin: false
    }
  },
  // 办理问卷
  {
    path: '/immediatelyTransaction',
    name: 'immediatelyTransaction',
    component: () =>
      import(/* webpackChunkName: "weChatActivity" */ '@/views/mobile/immediatelyTransaction/index.vue'),
    meta: {
      title: '立即办理',
      icon: 'iconquxiantu',
      hasLogin: false
    }
  },
  // 办理成功
  {
    path: '/successPage',
    name: 'successPage',
    component: () =>
      import(/* webpackChunkName: "weChatActivity" */ '@/views/mobile/successPage/index.vue'),
    meta: {
      title: '办理成功',
      icon: 'iconquxiantu',
      hasLogin: false
    }
  },
  // 预约
  {
    path: '/appointment',
    name: 'appointment',
    component: () =>
      import(/* webpackChunkName: "weChatActivity" */ '@/views/mobile/appointment/index.vue'),
    meta: {
      title: '预约',
      icon: 'iconquxiantu',
      hasLogin: false
    }
  },

  // {
  //   path: '/onekeyCopyPage',
  //   name: 'onekeyCopyPage',
  //   component: () =>
  //     import(/* webpackChunkName: "weChatActivity" */ '@/views/mobile/onekeyCopyPage/index.vue'),
  //   meta: {
  //     title: '一键登录测试',
  //     icon: 'iconquxiantu',
  //     hasLogin: false
  //   }
  // },

]

export default routes
