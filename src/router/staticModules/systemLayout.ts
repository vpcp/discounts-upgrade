import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'systemLayout'

const routes: Array<RouteRecordRaw> = [
  // {
  //   path: '/systemLayout',
  //   name: routeName,
  //   redirect: '/systemLayout/adConfig',
  //   component: RouterTransition,
  //   meta: {
  //     title: '系统配置',
  //     icon: 'iconsettings-line'
  //   },
  //   children: [
  //     {
  //       path: 'adConfig',
  //       name: `${routeName}-adConfig`,
  //       meta: {
  //         title: '广告位配置管理',
  //         icon: 'iconhotel-line'
  //       },
  //       component: () =>
  //         import(
  //           /* webpackChunkName: "dashboard-adConfig" */ '@/views/background/systemLayout/advertisingConfig.vue'
  //         )
  //     },
  //     {
  //       path: 'businessConfig',
  //       name: `${routeName}-businessConfig`,
  //       meta: {
  //         title: '业务配置管理',
  //         icon: 'iconchat-smile-2-line'
  //       },
  //       component: () =>
  //         import(
  //           /* webpackChunkName: "dashboard-welcome" */ '@/views/background/systemLayout/businessConfig.vue'
  //         )
  //     },
  //     {
  //       path: 'configDetail/:id/:isCheck?',
  //       name: `${routeName}-configDetail`,
  //       meta: {
  //         hidden: true,
  //         title: '业务配置详情',
  //         icon: 'iconchat-smile-2-line'
  //       },
  //       component: () =>
  //         import(
  //           /* webpackChunkName: "dashboard-welcome" */ '@/views/background/systemLayout/configDetail.vue'
  //         )
  //     }
  //   ]
  // }
]

export default routes
