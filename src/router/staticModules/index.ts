import dashboard from './dashboard'
import redirect from './redirect'
import systemLayout from './systemLayout'
import bordereaux from './bordereaux'

export default [...dashboard, ...redirect, ...systemLayout, ...bordereaux]
