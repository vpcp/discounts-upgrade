import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

export const routeName = 'dashboard'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/dashboard',
    name: routeName,
    redirect: '/dashboard/welcome',
    component: RouterTransition,
    meta: {
      title: '系统看板',
      icon: 'iconshuju1'
    },
    children: [
      {
        path: 'welcome',
        name: `${routeName}-welcome`,
        meta: {
          title: '首页',
          icon: 'iconhome-5-fill'
        },
        component: () =>
          import(
            /* webpackChunkName: "dashboard-welcome" */ '@/views/background/shared/dashboard/welcome/index.vue'
          )
      },
      // {
      //   path: 'autoCode',
      //   name: `${routeName}-autoCode`,
      //   meta: {
      //     title: '首页',
      //     icon: 'iconhome-5-fill'
      //   },
      //   component: () =>
      //     import(
      //       /* webpackChunkName: "dashboard-welcome" */ '@/views/background/autoCode/index.vue'
      //     )
      // },
      {
        path: 'importExcel',
        name: `importExcel`,
        meta: {
          title: '导入数据',
          icon: 'iconhome-5-fill',
          hidden: true
        },
        component: () =>
          import(
            /* webpackChunkName: "dashboard-welcome" */ '@/views/background/importExcel/index.vue'
          )
      }
    ]
  }
]

export default routes
