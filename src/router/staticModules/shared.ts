import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

/**
 * 不需要授权就可以访问的页面
 */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    component: () =>
      import(/* webpackChunkName: "login" */ '@/views/background/shared/login/index.vue')
  }
]

export default routes
