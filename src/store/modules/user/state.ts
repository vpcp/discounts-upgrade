import { Storage } from '@/utils/Storage'
import {
  ACCESS_TOKEN,
  CURRENT_USER,
  MOBILE_TOKEN,
  MOBILE_USER,
  ADMIN_USER
} from '@/store/mutation-types'

export type IUserState = {
  adminToken: string
  mobileToken: string
  name: string
  isMicroEnterpriseUsers: Boolean
  welcome: string
  avatar: string
  roles: any[]
  info: any
  mobileInfo: any
  adminInfo: any
}

export const state: IUserState = {
  isMicroEnterpriseUsers: false,
  adminToken: Storage.get(ACCESS_TOKEN, ''),
  adminInfo: Storage.get(ADMIN_USER, ''),
  mobileToken: Storage.get(MOBILE_TOKEN, ''),
  mobileInfo: Storage.get(MOBILE_USER, ''),
  name: '',
  welcome: '',
  avatar: '',
  roles: [],
  info: Storage.get(CURRENT_USER, {})
}
