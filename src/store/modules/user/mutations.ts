import { IUserState } from './state'

export const mutations = {
  setAdminToken: (state: IUserState, token: string) => {
    state.adminToken = token
  },
  setAdminInfo: (state: IUserState, adminInfo: string) => {
    state.adminInfo = adminInfo
  },
  setMobileToken: (state: IUserState, token: string) => {
    state.mobileToken = token
  },
  setAvatar: (state: IUserState, avatar: string) => {
    state.avatar = avatar
  },
  setRoles: (state: IUserState, roles) => {
    state.roles = roles
  },
  setUserInfo: (state: IUserState, info) => {
    state.info = info
  },
  setMobileInfo: (state: IUserState, info) => {
    state.mobileInfo = info
  },
  setIsMicroEnterpriseUsers: (state: IUserState, val) => {
    state.isMicroEnterpriseUsers = val
  }
}
