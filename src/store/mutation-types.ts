export const ACCESS_TOKEN = 'token' // 用户token
export const MOBILE_TOKEN = 'mobile_token' // 用户token
export const USER_UID = 'mobile_unionid' // 用户token
export const CURRENT_USER = 'userInfo' // 当前用户信息
export const ADMIN_USER = 'admin_userInfo' // 当前用户信息
export const MOBILE_USER = 'mobile_userInfo' // 当前用户信息
export const IS_LOCKSCREEN = 'Is-Lockscreen' // 是否锁屏
export const TABS_ROUTES = 'Tabs-Routes' // 标签页
