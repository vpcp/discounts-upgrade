import devConfig from './config.dev'
import devproxyConfig from './config.devproxy'

import proConfig from './config.pro'
import proproxyConfig from './config.proproxy'
export const MODE = import.meta.env.VITE_MODE as string
console.log('%c 🍋 MODE: ', 'font-size:20px;background-color: #FCA650;color:#fff;', MODE)

export const currentMode = {
  dev: devConfig,
  devproxy: devproxyConfig,
  pro: proConfig,
  proproxy: proproxyConfig
}
console.log(
  '%c 🍋 MODE: ',
  'font-size:20px;background-color: #FCA650;color:#fff;',
  currentMode[MODE].baseUrl
)
const config = {
  //版本号
  version: '1.0',
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: '社会渠道传播方案',
  /**
   * @description 缓存中存储的天数，默认3天
   */
  DEFAULT_CACHE_TIME: 60 * 60 * 24 * 3,

  /**
   * @description api请求基础路径
   */
  configureWebpack: (config) => {
    // 取消console打印
    config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
  },
  baseUrl: currentMode[MODE].baseUrl,
  vpUrl: currentMode[MODE].vpUrl,
  jplUrl: currentMode[MODE].jplUrl,
  webAppUrl: currentMode[MODE].webAppUrl,
  appid: currentMode[MODE].appid,
  activityId: currentMode[MODE].activityId,
  serviceId: currentMode[MODE].serviceId,
  publicPath: currentMode[MODE].publicPath,
  // 验证手机号
  validatePhoneRule: /^(?:(?:\+|00)86)?1\d{10}$/,
  // 固话
  validateTelePhoneRule: /^0\d{2,3}-?\d{7,8}$/,

  // 验证邮箱
  validateEmailRule:
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  ///^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/,

  /**
   * @description 默认打开的首页的路由name值，默认为home
   */
  homeName: 'home'
}

export default config
