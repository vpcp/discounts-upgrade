import devConfig from './config.dev'
export default {
  ...devConfig,
  //电脑系统开启代理 node就不能配置反向代理，把下面的baseurl注释掉即可，但是会存在跨域的问题
  ...{
    baseUrl: '/dev/'
  }
}
