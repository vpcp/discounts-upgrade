import proConfig from './config.pro'
export default {
  ...proConfig,
  ...{
    baseUrl: '/pro/'
  }
}
