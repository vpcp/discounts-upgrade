export default {
  appid: 'cxc1dae831643e4319',
  activityId: 'vpzhcjyxgj',
  serviceId: 'ability_customize',
  baseUrl: 'https://gd.10086.cn/apph5/openapi/app/handle',
  oneKeyPhone: 'https://gd.10086.cn/apph5/openapi/acquire/getAutoLoginPhone',
  webAppUrl: 'https://gd.10086.cn/apph5/openapi/behaviorlog/pushtrack.do',
  vpUrl: 'https://testcloud.vpclub.cn',
  jplUrl: 'https://jpl-api.besteast.cn/home/v2/',
  publicPath: '/apph5/vpzhcjyxgj/'
}
