import type { App } from 'vue'

import { AButton } from '@/components/button/index'

import Antd from 'ant-design-vue'

import 'ant-design-vue/dist/antd.css'

export function setupAntd(app: App<Element>) {
  app.component('AButton', AButton)

  app.use(Antd)
}
