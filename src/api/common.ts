import http from '@/utils/http/axios'
import { RequestEnum } from '@/enums/httpEnum'
import config from '@/config'

export default {
  // 获取验证码
  getYzm(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceId: 'ability_acquire_send_code'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 校验白名单
  checkWhite(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceId: 'query_black_white'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 校验白名单
  exchangePhone(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F7c55828811dc82f'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 获取验证码

  //上传文件
  uploadFile(data) {
    return http.request(
      {
        url: '/moses/upload/file/upload',
        typeUrl: 'vpUrl',
        method: RequestEnum.POST,
        data: data
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  //上传文件
  getJpl(data) {
    return http.request(
      {
        url: 'get_company',
        typeUrl: 'jplUrl',
        method: RequestEnum.GET,
        data: data
      },
      {
        isShowErrorMessage: true
      }
    )
  }
}
