import http from '@/utils/http/axios'
import { RequestEnum } from '@/enums/httpEnum'

export default {
  // 一键登陆,token换手机号码
  setToken(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F7daf3ce4b084083'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 发送验证码
  sendCode(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fe0b5656b7b69829'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 登陆,token换手机号码
  messagelogin(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F4d83a9ec94f52ea'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 问卷调查记录Fef62096c728abea
  getIssue(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fef62096c728abea'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 新增预约记录Fbae7884520bda29
  getSubscribe(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fbae7884520bda29'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 新增点击记录
  getClick(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F815871220c82f00'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
}
