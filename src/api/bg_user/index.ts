import http from '@/utils/http/axios'
import { RequestEnum } from '@/enums/httpEnum'

export default {
  editSysConfig(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F49b196684003a81'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  addBusConfig(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fbe8e07f82f1f464'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  queryLog(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F8adaf14f0769b7d'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  queryRecord(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F45a496440ed8202'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  editBusConfig(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F78c4a7e63edc00d'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  busList(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F5abb991dfbdb45b'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  standUp(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F2d085d5c059c699'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  queryBusConfig(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F01540ee8bb3fe04'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  yzmLogin(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Ffeb248ffbc9f386'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  addSysConfig(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fed66cae01e96905'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  addUser(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Ff7ac62aaa7c580d'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  checkUser(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fb617d543d20da1a'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  queryConfigDetail(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F0b1ef801066f0c5'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 导入接口
  importData(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fe56a07979ae3b49'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  importWhiteData(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fa7376bfdef3525d'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  activityList(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F311063d92149e54'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 查点击列表
  checkclicklist(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'Fcdf30b63136a635'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  //查问卷调查表
  checkquestionlist(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F8913fccdcdb9671'
      },
      {
        isShowErrorMessage: true
      }
    )
  },
  // 查套餐办理结果
  checkbuslist(data) {
    return http.request(
      {
        url: '',
        method: RequestEnum.POST,
        data,
        serviceCode: 'F2ba44756bd27af3'
      },
      {
        isShowErrorMessage: true
      }
    )
  }
}
