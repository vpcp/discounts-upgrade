import { formatDate, withInstall } from '@/utils/common'
import impExcel from './src/ImportExcel.vue'
import { jsonToSheetXlsx } from './src/Export2Excel'
// import expExcelModal from '@/ExportExcelModal.vue'

export const ImpExcel = withInstall(impExcel)
// export const ExpExcelModal = withInstall(expExcelModal)

/**
 * @description 导出数据
 * @param {Array} columns 表格头部信息
 * @param {Array} data 后台返回的数据
 * @param {string} timeKey 需要转化的时间key
 * @param {string} filename 导出的文件名称
 */
export const erportXlsx = (
  columns: any[],
  data: any[],
  timeKey = '',
  filename = '数据报表.xlsx'
) => {
  const head = {}
  const dArr: any = []
  columns.map((item: any) => {
    head[item.dataIndex] = item.title
  })
  data.map((ditem: any) => {
    if (timeKey) {
      ditem[timeKey] = formatDate(ditem[timeKey])
    }
    const obj = {}
    Object.keys(head).map((hitem) => {
      obj[hitem] = ditem[hitem] ? ditem[hitem] : ''
    })
    dArr.push(obj)
  })

  jsonToSheetXlsx({
    data: dArr,
    header: head,
    filename: filename,
    json2sheetOpts: {
      // 指定顺序
      // header: ['name', 'id']
    }
  })
}
export * from './src/typing'
export { jsonToSheetXlsx, aoaToSheetXlsx } from './src/Export2Excel'
