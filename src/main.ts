// import './publicPath'
import { createApp } from 'vue'
import App from './App.vue'
import VConsole from 'vconsole'
import { getIsMobile } from '@/utils/common'

import router, { setupRouter } from './router'
import { setupStore } from '@/store'
import useFormModal from '@/hooks/useFormModal'
import useCreateModal from '@/hooks/useCreateModal'
import VueHighlightJS from 'vue3-highlightjs'
import 'highlight.js/styles/Atom-One-Dark.css'
import { VueClipboard } from '@soerenmartius/vue3-clipboard'

// @ts-ignore
import { setupAntd, setupDirectives, setupGlobalMethods, setupCustomComponents } from '@/plugins'
import Vant from 'vant'
import 'vant/lib/index.css'
const app = createApp(App)
app.use(VueClipboard)
app.use(useFormModal)
app.use(useCreateModal)
app.use(VueHighlightJS)
app.use(Vant)
if (getIsMobile()) {
  // new VConsole({})
}

// 注册全局常用的ant-design-vue组件
setupAntd(app)
// 注册全局自定义组件,如：<svg-icon />
setupCustomComponents(app)
// 注册全局自定义指令，如：v-permission权限指令
setupDirectives(app)
// 注册全局方法，如：app.config.globalProperties.$message = message
setupGlobalMethods(app)
// 挂载vuex状态管理
setupStore(app)
// 挂载路由
setupRouter(app)
// 路由准备就绪后挂载APP实例
router.isReady().then(() => app.mount('#app'))
