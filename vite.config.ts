import type { UserConfig, ConfigEnv } from 'vite'
import { loadEnv } from 'vite'
import path from 'path'
import vueJsx from '@vitejs/plugin-vue-jsx'
import legacy from '@vitejs/plugin-legacy'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import styleImport from 'vite-plugin-style-import'
import compressPlugin from 'vite-plugin-compression'
import viteImagemin from 'vite-plugin-imagemin'

const CWD = process.cwd()

// 环境变量
const BASE_ENV_CONFIG = loadEnv('', CWD)
const DEV_ENV_CONFIG = loadEnv('development', CWD)
const PROD_ENV_CONFIG = loadEnv('production', CWD)

export default ({ command, mode }: ConfigEnv): UserConfig => {
  // 环境变量
  const { VITE_BASE_URL, VITE_DROP_CONSOLE, VITE_OUT_DIR } = loadEnv(mode, CWD)
  const env = loadEnv(mode, __dirname)
  // const isBuild = command === 'build';

  return {
    base: VITE_BASE_URL,
    // esbuild: {
    //   target: 'es2015'
    // },
    resolve: {
      alias: [
        {
          find: '@',
          replacement: resolve(__dirname, './src')
        },
        {
          find: '@c',
          replacement: resolve(__dirname, './src/components')
        },
        {
          find: '@m',
          replacement: resolve(__dirname, './src/mobileComponents')
        }
      ]
    },
    plugins: [
      viteImagemin({
        gifsicle: {
          optimizationLevel: 7,
          interlaced: false
        },
        optipng: {
          optimizationLevel: 7
        },
        mozjpeg: {
          quality: 20
        },
        pngquant: {
          quality: [0.8, 0.9],
          speed: 4
        },
        svgo: {
          plugins: [
            {
              name: 'removeViewBox'
            },
            {
              name: 'removeEmptyAttrs',
              active: false
            }
          ]
        }
      }),
      //开启打包压缩
      compressPlugin({
        ext: '.gz', //gz br
        algorithm: 'gzip', //brotliCompress gzip
        deleteOriginFile: false // 不删除源文件
      }),
      vue(),
      vueJsx({
        // options are passed on to @vue/babel-plugin-jsx
      }),
      legacy({
        targets: ['defaults', 'not IE 11']
      }),
      styleImport({
        libs: [
          {
            libraryName: 'ant-design-vue',
            esModule: true,
            resolveStyle: (name) => {
              return `ant-design-vue/es/${name}/style/index`
            }
          }
        ]
      })
    ],
    css: {
      preprocessorOptions: {
        less: {
          modifyVars: {},
          javascriptEnabled: true
        },
        scss: {
          additionalData: `
          @use 'sass:math';
          @import "src/styles/global.scss";
          `
        }
      }
    },
    server: {
      host: true,
      port: 8088,
      proxy: {
        // '/api': {
        //   target: 'http://29135jo738.zicp.vip',
        //   // target: 'http://localhost:7001',
        //   changeOrigin: true,
        //   rewrite: (path) => path.replace(/^\/api/, '/api/v1')
        // }
        '/dev': {
          target: 'https://gd.10086.cn/apph5/openapi/app/handle',
          ws: true,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/dev/, '')
        },
        '/pro': {
          target: 'https://gd.10086.cn/apph5/openapi/app/handle',
          ws: true,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/pro/, '')
        }
      }
    },
    optimizeDeps: {
      include: [
        '@ant-design/icons-vue',
        'ant-design-vue/es/locale/zh_CN',
        'ant-design-vue/es/locale/en_US'
      ],
      exclude: ['vue-demi']
    },
    build: {
      // brotliSize: false, //关闭打包计算
      outDir: VITE_OUT_DIR,
      assetsDir: 'static/img/',
      rollupOptions: {
        output: {
          chunkFileNames: 'static/js/[name]-[hash].js',
          entryFileNames: 'static/js/[name]-[hash].js',
          assetFileNames: 'static/[ext]/[name]-[hash].[ext]'
        }
      },
      terserOptions: {
        compress: {
          keep_infinity: true
          // drop_console: VITE_DROP_CONSOLE
        }
      }
    }
  }
}
