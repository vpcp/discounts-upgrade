module.exports = {
  plugins: {
    'postcss-px-to-viewport-update': {
      unitToConvert: 'px',
      viewportWidth: 375, //按设计稿宽度750像素来切图
      unitPrecision: 5,
      propList: ['*'],
      viewportUnit: 'vw',
      fontViewportUnit: 'vw',
      selectorBlackList: [],
      minPixelValue: 1,
      mediaQuery: false,
      replace: true,
      // exclude: [/src/], //忽略某些文件夹下的文件
      include: [
        /[\\/]src[\\/]views[\\/]mobile[\\/]/,
        /[\\/]src[\\/]mobileComponents[\\/]/,
        /[\\/]node_modules[\\/]vant[\\/]/
      ], //[/\/src\/views\/mobile\//]
      landscape: false,
      landscapeUnit: 'vw',
      landscapeWidth: 568
    }
  }
}
